const express = require('express');

const router = express.Router();

const BlogPost = require('../models/blogPost');
const BlogPostSub = require('../models/blogPostSub');

//writing REST APIs

//1.get a list for addition
router.get('/', (req, res) =>{

    BlogPost.find({  })
    .then((data) => {
console.log('Data', data);
res.json(data);
    })
    .catch((error) => {
        console.log('error', dataerror);  
   
     })
    });

//2.POST a new data to db for addition
router.post('/save', (req, res) =>{
console.log('Body', req.body);
 const data = req.body;
const newBlogPost = new BlogPost(data);
   //save
   newBlogPost.save((error) => {
       if(error) {
           res.status(500).json({msg:'Sorry, error'});
           return;
        } 
       return res.json({
            msg:'Your data has been saved'})
   });  
});



//3.UPDATE a data in the db for addition
router.put('/update/:id', (req, res) =>{
    BlogPost.findByIdAndUpdate({_id: req.params.id},req.body).then(function(){
  BlogPost.findOne({_id: req.params.id}).then(function(blogpost){
    res.send(blogpost);
  })
    

    });
   
});

//4.DELETE the data from db for addition
    router.delete('/delete/:id', (req, res) =>{
        BlogPost.findByIdAndRemove({_id: req.params.id}).then(function(blogpost){
       res.send(blogpost);

        })
        
         
        });


//1.get a list for subtraction
router.get('/getSub', (req, res) =>{

    BlogPostSub.find({  })
    .then((data) => {
console.log('Data', data);
res.json(data);
    })
    .catch((error) => {
        console.log('error', dataerror);  
   
     })
    });
//1.POST a list of subtraction data 
router.post('/sub', (req, res) =>{
    console.log('Body', req.body);
     const data = req.body;
    const newBlogPostSub = new BlogPostSub(data);
       //save
       newBlogPostSub.save((error) => {
           if(error) {
               res.status(500).json({msg:'Sorry, error'});
               return;
            }
           return res.json({
                msg:'We received your data'})
       });
    });


//3.UPDATE a data in the db of subtraction 
router.put('/updateSub/:id', (req, res) =>{
    BlogPostSub.findByIdAndUpdate({_id: req.params.id},req.body).then(function(){
  BlogPostSub.findOne({_id: req.params.id}).then(function(blogpostsub){
    res.send(blogpostsub);
  })
    

    });
   
});

//4.DELETE the data from db for subtraction 
    router.delete('/deleteSub/:id', (req, res) =>{
        BlogPostSub.findByIdAndRemove({_id: req.params.id}).then(function(blogpostsub){
       res.send(blogpostsub);

        })
        
         
        });









router.get('/name', (req, res) =>{
    const data = {
        date: '2020/02/03',
        daystodd: 2
    };
    res.json(data);   
});

module.exports = router;