import React, {Component} from 'react';
import { Button, DatePicker, Radio,InputNumber, Card,  Form, Input, Select } from 'antd';
import './App.css';
import {createStore, combineReducers } from 'redux';
import { Provider, connect } from 'react-redux';
import { Layout, Avatar, Breadcrumb,Anchor,  Collapse } from 'antd';
import { UserOutlined,TeamOutlined } from '@ant-design/icons';
import Title from 'antd/lib/typography/Title';
import { Menu } from 'antd';
import { BrowserRouter as Router } from 'react-router-dom';
import Route from 'react-router-dom/Route';
import { Link } from 'react-router-dom';
import moment from 'moment';
import axios from 'axios';
import { 
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  VideoCameraOutlined,
  UploadOutlined,
  PlusOutlined,
  MinusOutlined 
} from '@ant-design/icons';
const { Panel } = Collapse;
const { SubMenu } = Menu;
const { RangePicker } = DatePicker;
const { Option } = Select;
const { Header, Footer, Sider, Content } = Layout;
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};








//calander
/*class PickerSizesDemo extends React.Component {
state = {
    
    size: 'default',
    
        };

  handleSizeChange = e => {
    this.setState({ size: e.target.value });
  

    console.log(PickerSizesDemo);
  };

  render() {
    const { size } = this.state;
    
    return (
      <>
        
        <br />
        <br />
        <DatePicker size={size} />
        <br />
        
      </>
    
    );
  }

  newMethod() {
    console.log(DatePicker);
  }
}*/

function onChange(date, dateString) {
  console.log(date, dateString);
}


class SiderDemo extends React.Component {


  
state={num1:''
,num2:'',
total:'',
total1:'', 
date:"" ,
posts: [],
postSub: []
}
 



  //Addition
   exe1()
  {
    const date = this.state.date;
    this.setState({ total: date.add(this.state.num1, 'days').format("DD MMMM YYYY")})
  }
   //Sub
  exe2()
  {
    const date = this.state.date;

    this.setState({ total1: date.subtract(this.state.num2, 'days').format("DD MMMM YYYY")})
  }
 
//form submitfor addition
submit = (event) =>{
  event.preventDefault();
  const payload = {
    datepicker: this.state.date,
    daystoadd: this.state.num1 ,
    Addition: this.state.total
  };
  
axios({
  url: '/api/save',
  method: 'POST',
  data: payload
})
.then(() => {
console.log('Data has been saved');
this.resetUserInputs();
this.getBlogPost();
})
.catch(() => {
  console.log('Data has been not saved');
});;

};

resetUserInputs = () => {
  this.setState({
    date:'',
   num1: '' ,
    total:''
  })
}


  state = {
    collapsed: false,
  };


  componentDidMount = () => {
    //this.getBlogPost();
   // this.getBlogPostSub();   
  }
 //Retrive Data for addition
  getBlogPost = () => {
    axios.get('/api')
    .then((response) => {
      const data = response.data;
      this.setState({ posts:data});
console.log('Data has been recieved');
    })
    .catch(() => {
      alert('Errorrrr');
          });
  }

  //Retrive Data for addition
  getBlogPostSub = () => {
    axios.get('api/getSub')
    .then((response) => {
      const data = response.data;
      this.setState({ postSub:data});
console.log('Sub-Data has been recieved');
    })
    .catch(() => {
      alert('Errorrrr');
          });
  }

  //delete data for addition
 deleteAdd(id) {
    axios.delete('/'+id)
    .then(res =>  console.log(res.data)); 
    this.setState({
      posts: this.state.posts.filter(el =>el._id != id)
    })

  }

  //form submit for subtraction
submits = (event) =>{
  event.preventDefault();
  const payload = {
    datepicker: this.state.date,
    daystosub: this.state.num2 ,
    Subtraction:this.state.total1
  };
  axios({
    url: 'api/sub',
    method: 'POST',
    data: payload
  })
  .then(() => {
  console.log('Data sent to server');
  this.resetSubInputs();
  this.getBlogPostSub();
  })
  .catch(() => {
    console.log('Data not saved');
  });;
};

resetSubInputs = () => {
  this.setState({
    date: '',
   num2: '' ,
    total1:''
  })
}
 /* handleChange = ({target}) => {
const target = event.target;
const name = target.name;
const value = target.value;
this.setState ({
  [name]: value
});
  };*/

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
 //for addition
displayBlogPost = (posts) => {
return posts && posts.map((post , index) => (
  <div key={index}  className=" blog-post_display">
    Date: {post.datepicker} + Days: {post.daystoadd} = {post.Addition}
    <p>{post.body}</p>
    </div>
))
};

//for subtraction
displayBlogPostSub = (postSub) => {
  return postSub && postSub.map((post1 , index) => (
    <div key={index} className="blog-postSub_display">
   Date: {post1.datepicker} + Days: {post1.daystosub} = {post1.Subtraction}
      </div>
  ))
  };
  


  render() {
    const {posts} = this.props;
    this.deleteAdd = this.deleteAdd.bind(this);
    console.log('State:', this.state);
    return (
   
    <Router>

      <Route path="/" method="GET" exact strict render = {
        () => {
          return (
            <form onSubmit={this.submit}>    
      <Layout>
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
          <div className="logo" />
            <Menu theme="dark" mode="inline" >
            <Menu.Item key="1"  icon={<PlusOutlined/>}>
             <span>Add</span>
                <Link to="/" />
               </Menu.Item>
                <Menu.Item key="2" icon={<MinusOutlined />}>
            <span>Sub</span>
                <Link to="/sub" />
                </Menu.Item>
               </Menu>
        </Sider>

        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: this.toggle, })}
          </Header>
         
          <Content
            className="site-layout-background"
            style={{
              margin: '20px 20px',
              padding: 61,
              minHeight: 500,
            }}>
           <center>
             
          <Avatar src='./blue.png' size="large" />
               <h1><b>Add to Date </b></h1>
               Select a number of days to add to the selected date.The resulting
               <p>date will show on the history section of both pages</p>
               </center>
              
            <center>
            <Collapse bordered={false} defaultActiveKey={['1']}>
            
            <br/><br/>
            
            <b><label>Date:</label></b>
            <p><DatePicker name="datepicker" value={this.state.date} onChange={(date, dateString) => {onChange(date, dateString); this.setState({ date: date}); }} />
            </p>
            <b> <label>Days to add:</label></b>
            <p><Input placeholder="Pick Date" name="daystoadd" size="default" style={{margin:'200px'}} style={{width: "200px"}}  allowClear onChange={onChange} className="inputStyle" value={this.state.num1} onChange={ (eve) => { this.setState({ num1: eve.target.value}) } } /></p>
            <br/><br/>
         </Collapse>
        
              <Button type="primary"  id="add" onClick={()=>{this.exe1()}}  style={{margin:'3px'}}>add</Button>
              <Button type="submit" onClick={this.submit}  style={{margin:'3px'}}  >Save to History</Button>
             
              <Input type="text" name="Addition" placeholder="Add Result"  value={this.state.total} />
              <div>
              <h1><b> History:</b></h1> {this.displayBlogPost(this.state.posts)}
                    
                </div>
            </center>
 
          </Content>
        </Layout>
      </Layout>
     </form>
      );}

        }/>

<Route path="/sub" method="GET" exact strict render = {
  () => {
    return (
     
       <Layout>
          <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
               <div className="logo" />
             <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                 <Menu.Item key="1" icon={<PlusOutlined />}>
                 <span>Add</span>
             <Link to="/" />
                  </Menu.Item>
            <Menu.Item key="2" icon={<MinusOutlined />}>
                  <span>Sub</span>
             <Link to="/sub" />
            </Menu.Item>
           </Menu>
       </Sider>
     <Layout className="site-layout">
        <Header className="site-layout-background" style={{ padding: 0 }}>
         {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
        className: 'trigger',
        onClick: this.toggle,
      })}
    </Header>
    <Content
      className="site-layout-background"
      style={{
        
        margin: '20px 20px',
              padding: 61,
              minHeight: 500,
      }}
    >
      <center>
      <center>
    <Avatar src='./red.png' size="large" />
         <h1><b>Subtract from Date </b></h1>
         Select a number of days to sub to the selected date.The resulting
         <p>date will show on the history section of both pages</p>
         </center>
         <Collapse bordered={false} defaultActiveKey={['1']}>
         <br/><br/>
         <b> <label>Date:</label></b>
         <p><DatePicker onChange={(date, dateString) => {onChange(date, dateString); this.setState({ date: date}); }}/></p>
             <b> <label>Days to Subtract:</label></b>
             <p>  <Input placeholder="Pick date"  size="default" style={{margin:'200px'}} style={{width: "200px"}}  className="inputStyle" value={this.state.num2} onChange={ (eve) => { this.setState({ num2: eve.target.value}) } } /></p>
            
              <br/><br/>

           </Collapse>
        <Button type="primary"  id="sub" onClick={()=>{this.exe2()}}    style={{margin:'3px'}}>Sub</Button>
        <Button htmlType="button" onClick={this.submits} style={{margin:'3px'}} >Save to History</Button>
        <Input type="text" placeholder="Sub Result"  value={this.state.total1} />

         <div>
         <h1><b> History:</b></h1> {this.displayBlogPostSub(this.state.postSub)}
                    
                </div>
      
      </center>

    </Content>
  </Layout>
</Layout>
    );
  }
  }/>

      </Router>
    );
  }
}
export default SiderDemo;

//ReactDOM.render(<SiderDemo />, mountNode);
