//import npm packages

const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const path = require('path');


const app = express();
const PORT = process.env.PORT || 8080;

const routes = require('../routes/api');

//const MONGODB_URI = 'mongodb+srv://devanshi:devanshidesai@datepickerdb.zjtvp.mongodb.net/<dbname>?retryWrites=true&w=majority'

mongoose.connect('mongodb://localhost/database', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
//devanshidesai
mongoose.connection.on('connected', () => {
    console.log('it is connected');
});
 app.use(express.json());
 app.use(express.urlencoded({ extended: false }));

app.use(morgan('tiny'));
app.use('/api', routes);
app.listen(PORT, console.log(`Server is starting at: ${PORT}`));